#Projeto DNA do ML (teste)

Requisitos: Jdk 8 ou superior, Maven 3.5.3 ou superior.

Para rodar os testes: mvn clean test
	
Para rodar o projeto: mvn clean spring-boot:run
	
Endpoints: 

http://localhost:9090/simian (POST)

http://localhost:9090/stats (GET)

Testes pelo curl: 

curl -X POST -H "Content-Type:application/json" -d "{\"dna\": [\"ATGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"]}" http://localhost:9090/simian

curl http://localhost:9090/stats
