package com.eduacsp.teste.mlteste;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.ui.ExtendedModelMap;

import com.eduacsp.teste.mlteste.app.Application;
import com.eduacsp.teste.mlteste.controller.DnaController;
import com.eduacsp.teste.mlteste.repo.DnaHistoryRepo;
import com.eduacsp.teste.mlteste.vo.DnaVo;
import com.eduacsp.teste.mlteste.vo.ReturnVo;

import junit.framework.TestCase;

@PropertySource("classpath:/application.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class AppTest extends TestCase{

	private static final Logger logger = LogManager.getLogger(DnaController.class);

	@Autowired
	private DnaController dnaController;

	@Autowired
	private DnaHistoryRepo dnaHistoryRepo;

	@Test
	public void validarInsercaoDnaComTamanhoMedio() {
		logger.info("validarInsercaoDnaComTamanhoMedio");

		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		dnaList.add("CTGAGA");
		dnaList.add("CTATGC");
		dnaList.add("TATTGT");
		dnaList.add("AGTAGG");
		dnaList.add("CCCCTA");
		dnaList.add("TCACTG");
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComTamanhoGrande() {
		logger.info("validarInsercaoDnaComTamanhoGrande");

		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");	
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaList.add("CTGAGAATATATATA");
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComTamanhoVazio() {
		logger.info("validarInsercaoDnaComTamanhoVazio");

		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.BAD_REQUEST, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComTamanhoNulo() {
		logger.info("validarInsercaoDnaComTamanhoNulo");

		DnaVo dnaVo = new DnaVo();
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.BAD_REQUEST, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComTamanhoPequeno() {
		logger.info("validarInsercaoDnaComTamanhoPequeno");

		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		dnaList.add("CT");
		dnaList.add("CT");
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.BAD_REQUEST, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComTamanhoDeLinhasEColunasDiferentes() {
		logger.info("validarInsercaoDnaComTamanhoDeLinhasEColunasDiferentes");

		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		dnaList.add("CTA");
		dnaList.add("CT");
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);

		assertEquals(HttpStatus.BAD_REQUEST, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComSimianHorizontal() {
		logger.info("validarInsercaoDnaComSimianHorizontal");

		ResponseEntity<Object> retorno = inserirDna("horizontal");

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComSimianVertical() {
		logger.info("validarInsercaoDnaComSimianVertical");

		ResponseEntity<Object> retorno = inserirDna("vertical");

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComSimianDiagonalDireita() {
		logger.info("validarInsercaoDnaComSimianVertical");

		ResponseEntity<Object> retorno = inserirDna("diagonalDireita");

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarInsercaoDnaComSimianDiagonalEsquerda() {
		logger.info("validarInsercaoDnaComSimianVertical");

		ResponseEntity<Object> retorno = inserirDna("diagonalEsquerda");

		assertEquals(HttpStatus.OK, retorno.getStatusCode());
	}

	@Test
	public void validarStatsComMaisHumanosQueSimios() {
		logger.info("validarStatsComMaisHumanosQueSimios");

		dnaHistoryRepo.deleteAll();

		inserirDna("humano1");

		inserirDna("humano2");

		inserirDna("horizontal");

		ResponseEntity<ReturnVo> retorno = dnaController.stats();
		
		BigDecimal bRatio = new BigDecimal(retorno.getBody().getRatio(), MathContext.DECIMAL64);

		assertTrue(bRatio.compareTo(new BigDecimal("1.0")) < 0);
	}

	@Test
	public void validarStatsComMaisSimiosQueHumanos() {
		logger.info("validarStatsComMaisSimiosQueHumanos");

		dnaHistoryRepo.deleteAll();

		inserirDna("humano1");
		
		inserirDna("vertical");

		inserirDna("horizontal");

		ResponseEntity<ReturnVo> retorno = dnaController.stats();
		
		BigDecimal bRatio = new BigDecimal(retorno.getBody().getRatio(), MathContext.DECIMAL64);
		
		assertTrue(bRatio.compareTo(new BigDecimal("1.0")) > 0);
	}
	
	@Test
	public void validarStatsComSimiosIguaisAHumanos() {
		logger.info("validarStatsComSimiosIguaisAHumanos");

		dnaHistoryRepo.deleteAll();

		inserirDna("humano1");
		
		inserirDna("vertical");

		ResponseEntity<ReturnVo> retorno = dnaController.stats();
		
		BigDecimal bRatio = new BigDecimal(retorno.getBody().getRatio(), MathContext.DECIMAL64);
		
		assertTrue(bRatio.compareTo(new BigDecimal("1.0")) == 0);
	}
	
	@Test
	public void validarStatsComSimiosSemHumanos() {
		logger.info("validarStatsComSimiosSemHumanos");

		dnaHistoryRepo.deleteAll();

		inserirDna("vertical");

		ResponseEntity<ReturnVo> retorno = dnaController.stats();
		
		//logger.info("retorno.getBody().getRatio():"+retorno.getBody().getRatio()+" | "+retorno.getBody().getCountHumanDna()+" | "+retorno.getBody().getCountMutantDna());
		
		BigDecimal bRatio = new BigDecimal(retorno.getBody().getRatio(), MathContext.DECIMAL64);
		
		assertTrue(bRatio.compareTo(new BigDecimal("0.0")) == 0);
	}
	
	@Test
	public void validarStatsComSimiosSemSimios() {
		logger.info("validarStatsComSimiosSemSimios");

		dnaHistoryRepo.deleteAll();

		inserirDna("humano1");

		ResponseEntity<ReturnVo> retorno = dnaController.stats();
		
		logger.info("retorno.getBody().getRatio():"+retorno.getBody().getRatio()+" | "+retorno.getBody().getCountHumanDna()+" | "+retorno.getBody().getCountMutantDna());
		
		BigDecimal bRatio = new BigDecimal(retorno.getBody().getRatio(), MathContext.DECIMAL64);
		
		assertTrue(bRatio.compareTo(new BigDecimal("0.0")) == 0);
	}



	private ResponseEntity<Object> inserirDna(String tipo) {
		DnaVo dnaVo = new DnaVo();
		List<String> dnaList = new ArrayList<String>();
		if("horizontal".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTGTGC");
			dnaList.add("TATTGT");
			dnaList.add("AAAATG");
			dnaList.add("CGGCTA");
			dnaList.add("TCACTG");
		}else if("vertical".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTTTGC");
			dnaList.add("TATTGT");
			dnaList.add("GATGTG");
			dnaList.add("CGTCTA");
			dnaList.add("TCACTG");
		}else if("diagonalDireita".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTATGC");
			dnaList.add("TAGTGT");
			dnaList.add("GATGTG");
			dnaList.add("CTTCTA");
			dnaList.add("TCACTG");
		}else if("diagonalEsquerda".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTTTGC");
			dnaList.add("TAGTGT");
			dnaList.add("GATGTG");
			dnaList.add("CGTCTA");
			dnaList.add("TCACTG");
		}else if("humano1".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTGTGC");
			dnaList.add("TATTGT");
			dnaList.add("AGAGTG");
			dnaList.add("CGGCTA");
			dnaList.add("TCACTG");
		}else if("humano2".equals(tipo)) {
			dnaList.add("CTGAGA");
			dnaList.add("CTGTGC");
			dnaList.add("TATGGT");
			dnaList.add("AGAGTG");
			dnaList.add("CGGCTA");
			dnaList.add("TCACTG");
		}
		dnaVo.setDna(dnaList);
		ResponseEntity<Object> retorno = dnaController.simian(new ExtendedModelMap(), dnaVo);
		return retorno;
	}

}
