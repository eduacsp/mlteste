package com.eduacsp.teste.mlteste.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eduacsp.teste.mlteste.model.DnaHistory;
import com.eduacsp.teste.mlteste.vo.DnaCount;


@Repository
public interface DnaHistoryRepo extends CrudRepository<DnaHistory, Long>{

	@Query("select new com.eduacsp.teste.mlteste.vo.DnaCount(d.isSimian,count(d)) from com.eduacsp.teste.mlteste.model.DnaHistory d group by d.isSimian")
	public List<DnaCount> findDnaCount();
	

}
