package com.eduacsp.teste.mlteste.vo;

public class DnaCount {
	
	private long count;
	
	private boolean isSimian;
	
	public DnaCount() {}

	public DnaCount(boolean isSimian,long count) {
		this.isSimian = isSimian;
		this.count = count;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public boolean isSimian() {
		return isSimian;
	}

	public void setSimian(boolean isSimian) {
		this.isSimian = isSimian;
	}




	
	
	
}
