package com.eduacsp.teste.mlteste.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.eduacsp.teste.mlteste.service.DnaService;
import com.eduacsp.teste.mlteste.vo.DnaVo;
import com.eduacsp.teste.mlteste.vo.ReturnVo;

@Controller
public class DnaController {

	private static final Logger logger = LogManager.getLogger(DnaController.class);

	@Autowired
	private DnaService dnaService;

	@PostMapping(path="/simian", consumes="application/json")	
	public ResponseEntity<Object> simian(Model model,@RequestBody DnaVo dnaVo) {
		ResponseEntity<Object> returnBody = null;

		HttpStatus checkSaveReturn = dnaService.checkAndSave(dnaVo);

		returnBody = ResponseEntity.status(checkSaveReturn).body(null);

		logger.debug("simian: "+dnaVo.toString()+" | "+returnBody.toString());

		return returnBody;
	}

	@GetMapping(path="/stats",produces="application/json")	
	public ResponseEntity<ReturnVo> stats() {
		logger.debug("stats()");

		ReturnVo returnVo = dnaService.stats();

		ResponseEntity<ReturnVo> returnBody = ResponseEntity.status(HttpStatus.OK).body(returnVo);	

		logger.debug("stats: "+returnVo.toString()+" | "+returnBody.toString());

		return returnBody;
	}

}
