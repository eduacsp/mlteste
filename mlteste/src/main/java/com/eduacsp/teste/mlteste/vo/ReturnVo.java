package com.eduacsp.teste.mlteste.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReturnVo {

	@JsonProperty(value="count_mutant_dna")
	private long countMutantDna;
	
	@JsonProperty(value="count_human_dna")
	private long countHumanDna;
	
	private double ratio;

	public long getCountMutantDna() {
		return countMutantDna;
	}

	public void setCountMutantDna(long countMutantDna) {
		this.countMutantDna = countMutantDna;
	}

	public long getCountHumanDna() {
		return countHumanDna;
	}

	public void setCountHumanDna(long countHumanDna) {
		this.countHumanDna = countHumanDna;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	

	


	
	
	
	
	
}
