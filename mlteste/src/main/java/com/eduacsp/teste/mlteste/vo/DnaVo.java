package com.eduacsp.teste.mlteste.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DnaVo implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<String> dna = new ArrayList<String>();
	
	public List<String> getDna() {
		return dna;
	}
	public void setDna(List<String> dna) {
		this.dna = dna;
	}



	@Override
	public String toString() {
		return "DnaVo [dna=" + dna + "]";
	}
	
}
