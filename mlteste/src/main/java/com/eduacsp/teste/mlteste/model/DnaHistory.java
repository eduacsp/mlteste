package com.eduacsp.teste.mlteste.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="dna_history")
public class DnaHistory {
	
	@Id
	@Column(name="id_dna_hash_code", updatable = false, nullable = false)
	private Integer idDnaHashCode;

	@Column(name="is_simian")
	private boolean isSimian;
	
//	@OneToMany(mappedBy = "idDnaLine", targetEntity = DnaLine.class)
//	@Column(name="dnaLine")

	public boolean isSimian() {
		return isSimian;
	}

	public Integer getIdDnaHashCode() {
		return idDnaHashCode;
	}

	public void setIdDnaHashCode(Integer idDnaHashCode) {
		this.idDnaHashCode = idDnaHashCode;
	}

	public void setSimian(boolean isSimian) {
		this.isSimian = isSimian;
	}


	
	
}
