package com.eduacsp.teste.mlteste.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.eduacsp.teste.mlteste.model.DnaHistory;
import com.eduacsp.teste.mlteste.repo.DnaHistoryRepo;
import com.eduacsp.teste.mlteste.vo.DnaCount;
import com.eduacsp.teste.mlteste.vo.DnaVo;
import com.eduacsp.teste.mlteste.vo.ReturnVo;

@Service
public class DnaService {

	private static final Logger logger = LogManager.getLogger(DnaService.class);

	@Autowired
	private DnaHistoryRepo dnaHistoryRepo;

	public HttpStatus checkAndSave(DnaVo dnaVo) {
		logger.debug("checkAndSave");
		HttpStatus httpStatus = HttpStatus.FORBIDDEN;

		boolean validate = validateMatrix(dnaVo);

		if(validate) {
			int prime = 31;
			int result = 1;
			for( String dnaLine : dnaVo.getDna() ){
				result = result * prime + dnaLine.hashCode();
			}
			logger.debug("hash code dna: "+result);

			boolean isSimian = isSimian(convertToMatrix(dnaVo.getDna()));

			DnaHistory dnaHistory = new DnaHistory();
			dnaHistory.setIdDnaHashCode(result);
			dnaHistory.setSimian(isSimian);

			dnaHistoryRepo.save(dnaHistory);

			if(isSimian) {
				httpStatus = HttpStatus.OK;
			}
		}else {
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		return httpStatus;
	}


	private boolean validateMatrix(DnaVo dnaVo) {
		boolean validade = false;
		int lines = 0;
		int cols = 0;

		if( dnaVo.getDna()!=null &&  dnaVo.getDna().size()>0) {
			lines = dnaVo.getDna().size();
			for (String line : dnaVo.getDna()) {
				if(line.length() == lines) {
					validade = true;
					cols = line.length();
				}else {
					validade = false;				
					break;
				}
			}

			if(!validade) {
				logger.error("Número de linhas é diferente do número de colunas!");
				return false;
			}

			if(lines< 4 || cols < 4 ) {
				logger.error("Sequência de DNA muito pequena!");
				return false;
			}else {
				return true;
			}
		}else {
			logger.error("Sequência de DNA inválida!");
			return false;
		}
	}


	public ReturnVo stats() {
		ReturnVo returnVo = new ReturnVo();
		List<DnaCount> listDnaCount = dnaHistoryRepo.findDnaCount();
		double ratio = 0.0;

		for (DnaCount dnaCount : listDnaCount) {
			if(dnaCount.isSimian()) {
				returnVo.setCountMutantDna(dnaCount.getCount());
			}else {
				returnVo.setCountHumanDna(dnaCount.getCount());	
			}
		}

		if(returnVo.getCountHumanDna()>0) {
			ratio = returnVo.getCountMutantDna() * 1.0/returnVo.getCountHumanDna();
		}
		returnVo.setRatio(ratio);

		return returnVo;
	}



	boolean isSimian (String[] dna) {
		char[][] dnaMatrix = new char[dna.length][dna.length];
		boolean found = false;

		for (int i = 0; i < dna.length; i++) {
			char[] dnaToChar = dna[i].toCharArray();
			for (int j = 0; j < dnaToChar.length; j++) {
				logger.debug("("+i+","+j+") = "+j);
				dnaMatrix[i][j] = dnaToChar[j];
			}
		}

		found = check(dnaMatrix, "vertical");
		logger.debug("found vertical: "+found);

		if(!found) {
			found = check(dnaMatrix, "horizontal");
			logger.debug("found horizontal: "+found);
		}
		if(!found) {
			found = check(dnaMatrix, "diagonal");
			logger.debug("found diagonal: "+found);
		}		

		return found;
	}

	private boolean check(char[][] dnaMatrix, String type) {
		boolean retornOk = false;

		if("horizontal".equals(type)) {

			retornOk = searchHorizontal(dnaMatrix);

		}else if("vertical".equals(type)) {

			retornOk = searchVertical(dnaMatrix);

		}else if("diagonal".equals(type)) {

			retornOk = searchDiagonalDir(dnaMatrix);

			if(!retornOk) {
				retornOk = searchDiagonalEsq(dnaMatrix);
			}
		}

		return retornOk;
	}


	private boolean searchVertical(char[][] dnaMatrix) {
		boolean retornOk = false;
		int dnaEqual = 0;
		outerloop:
			for (int j = 0; j < dnaMatrix.length; j++) {//coluna
				for (int i = 0; i < dnaMatrix.length; i++) {//linha
					if((i + 1) < dnaMatrix.length && dnaMatrix[i][j] == dnaMatrix[(i+1)][j]) {
						dnaEqual++;
					}else {
						dnaEqual=0;
					}
					if(dnaEqual >=3) {
						retornOk = true;
						break outerloop;
					}
				}
			}
		return retornOk;
	}


	private boolean searchHorizontal(char[][] dnaMatrix) {
		boolean retornOk = false;
		int dnaEqual = 0;
		outerloop:
			for (int i = 0; i < dnaMatrix.length; i++) {//linha
				for (int j = 0; j < dnaMatrix.length; j++) {//coluna
					if((j + 1) < dnaMatrix.length && dnaMatrix[i][j] == dnaMatrix[i][(j + 1)]) {
						dnaEqual++;
					}else {
						dnaEqual=0;
					}
					if(dnaEqual >=3) {
						retornOk = true;
						break outerloop;
					}
				}
			}
		return retornOk;
	}

	private boolean searchDiagonalEsq(char[][] dnaMatrix) {
		int innerCount = 0;
		int column = dnaMatrix.length;
		int height = 0;
		int line = dnaMatrix.length-1;
		boolean retornOk = false;
		int dnaEqual = 0;

		outerloop:
			for (int i = ((dnaMatrix.length-1)+(dnaMatrix.length-1)); i >=0; i--) {
				if(innerCount>=(dnaMatrix.length)) {
					line--;
					height--;
				}else {
					height++;
				}
				boolean first = true;
				int lineInner = line;
				for (int j = 0; j < height; j++) {
					if(first) {
						column = i - line;
					}else {
						++column;
					}
					if((lineInner-1)>= 0 
							&& (column+1)<= (dnaMatrix.length-1) 
							&& dnaMatrix[lineInner][column]==dnaMatrix[(lineInner-1)][(column+1)]) {
						dnaEqual++;
						if(dnaEqual>=3) {
							retornOk = true;
							break outerloop;
						}
					}else {
						dnaEqual = 0;
					}
					first = false;
					lineInner--;
				}
				innerCount++;
			}

		return retornOk;
	}

	private boolean searchDiagonalDir(char[][] dnaMatrix) {
		int column = dnaMatrix.length;
		int line =0;
		int innerCount=0;
		int height = 0;
		boolean retornOk = false;
		int dnaEqual = 0;

		outerloop:
			for (int i = 0; i <=((dnaMatrix.length-1)+(dnaMatrix.length-1)); i++) {
				if(innerCount>=(dnaMatrix.length)) {
					line++;
					height--;
				}else {
					height++;
				}
				boolean first = true;
				int lineInner = line;

				for (int j = 0; j < height; j++) {
					if(first) {
						if(((dnaMatrix.length-1) - i)>=0) {
							column = (dnaMatrix.length-1) - i;
						}else {
							column = 0;
						}
					}else {
						column++;
					}

					if(((lineInner+1)<= (dnaMatrix.length-1) 
							&& (column+1)<= (dnaMatrix.length-1) 
							&& dnaMatrix[lineInner][column]==dnaMatrix[(lineInner+1)][(column+1)])) {
						dnaEqual++;
						if(dnaEqual>=3) {
							retornOk = true;
							break outerloop;
						}
					}else {
						dnaEqual = 0;
					}
					lineInner++;
					first = false;
				}
				innerCount++;
			}

		return retornOk;
	}


	private String[] convertToMatrix(List<String> dna) {

		String[] itemsArray = new String[dna.size()];

		if(dna.size()>0) {
			itemsArray = dna.toArray(itemsArray);
		}

		return itemsArray;
	}

}
